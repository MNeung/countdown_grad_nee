import React, { useState, useEffect } from 'react';

const Main = () => {

    // let [secValue, secFun] = useState(0);
    // let [minValue, minFun] = useState(0);
    // let [hourValue, hourFun] = useState(0);
    // // let [dayValue, dayFun] = useState(0);

    // useEffect(() => {
    //     let countdownNum = setInterval(() => {
    //         // Set the date we're counting down to
    //         var countDownDate = new Date("Jun 23, 2020 00:00:00").getTime();
    //         // Get today's date and time
    //         var now = new Date().getTime();

    //         // Find the distance between now and the count down date
    //         var distance = countDownDate - now;

    //         // Time calculations for days, hours, minutes and seconds
    //         // var days = Math.floor(distance / (1000 * 60 * 60 * 24));
    //         var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    //         var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    //         var seconds = Math.floor((distance % (1000 * 60)) / 1000);

    //         // dayFun(days);
    //         hourFun(hours);
    //         minFun(minutes);
    //         secFun(seconds);
    //     }, 1000);
    //     return () => clearInterval(countdownNum);
    // });

    // return (
    //     <div>
    //         {/* <div>
    //             <div>day</div>
    //             <div>{dayValue}</div>
    //         </div> */}
    //         <div>
    //             <div>hour</div>
    //             <div>{hourValue}</div>
    //         </div>
    //         <div>
    //             <div>minute</div>
    //             <div>{minValue}</div>
    //         </div>
    //         <div>
    //             <div>second</div>
    //             <div>{secValue}</div>
    //         </div>
    //     </div>
    // );

    const [counter, setCounter] = React.useState(60);

    // Third Attempts
    React.useEffect(() => {
        const timer =
            counter > 0 && setInterval(() => setCounter(counter - 1), 1000);
        return () => clearInterval(timer);
    }, [counter]);

    return (
        <div className="App">
            <div>Countdown: {counter}</div>
        </div>
    );
}

export default Main;